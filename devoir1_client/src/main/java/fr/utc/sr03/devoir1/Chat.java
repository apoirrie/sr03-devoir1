package fr.utc.sr03.devoir1;

/**
 * Class used to display messages in the terminal
 */
public class Chat {

    public Chat() {

    }

    /**
     * Format and display a user message in the terminal
     * @param pseudo pseudo of the sender
     * @param message content of the message
     */
    public void displayMessage(String pseudo, String message) {
        String coloredMessage = "\u001B[35m" + pseudo + " a dit : " + "\u001B[0m"; // Couleur violette
        System.out.println(coloredMessage + message);
       // System.out.println(pseudo + " à dit : " + message);
    }
}
