package fr.utc.sr03.devoir1;

/**
 * Entry point for the client application
 *
 */
public class AppClient {

    public static void main(String[] args) {
        Client client = new Client();
        client.start(14002);
    }
}
