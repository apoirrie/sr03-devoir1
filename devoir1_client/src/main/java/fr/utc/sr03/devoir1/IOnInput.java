package fr.utc.sr03.devoir1;

/**
 * Interface for handling user local input
 */
@FunctionalInterface
public interface IOnInput {
    /**
     * Method prototype for handling user input in the terminal
     * @param string what was typed in the terminal
     * @throws Exception
     */
    void onInput(String string) throws Exception;
}
