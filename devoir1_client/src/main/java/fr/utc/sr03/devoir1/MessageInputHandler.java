package fr.utc.sr03.devoir1;

import java.util.Scanner;

/**
 * Class responsible for the handling of the user input in the terminal.
 * This class is fully abstracted of the client/server system
 */
public class MessageInputHandler extends Thread {
    private Scanner scanner;
    private IOnInput onInputHandler;

    MessageInputHandler(Scanner scanner, IOnInput onInputHandler) {
        this.scanner = scanner;
        this.onInputHandler = onInputHandler;
    }

    @Override
    public void run() {
        while (true) {
            String message = scanner.nextLine();
            try {
                onInputHandler.onInput(message);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
