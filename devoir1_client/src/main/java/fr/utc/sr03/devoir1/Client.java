package fr.utc.sr03.devoir1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

import fr.utc.sr03.devoir1.common.ChatMessage;
import fr.utc.sr03.devoir1.common.PseudoMessage;
import fr.utc.sr03.devoir1.common.ServerResponse;

/**
 * Client implementation of the chat project. It handles server connection,
 * pseudo attribution, and message communication with the server.
 */
public class Client {
    private String pseudo = null;
    private Socket socket = null;
    private ObjectOutputStream out = null;
    private ObjectInputStream ins = null;
    private final Scanner scanner = new Scanner(System.in);
    private final Chat chat;
    private MessageInputHandler messageInputHandler;

    public Client() {
        chat = new Chat();
        messageInputHandler = new MessageInputHandler(scanner, this::sendMessage);
    }


    /**
     * Try to connect a socket to a distant server. It then initializes the communication objects,
     * do the pseudo attribution, and finally enter the chat while server is on or exit it typed
     * @param port port of the distant server
     */
    public void start(int port) {
        try {
            socket = new Socket("localhost", port);
            System.out.println("Connected to server");
            out = new ObjectOutputStream(socket.getOutputStream());
            ins = new ObjectInputStream(socket.getInputStream());
            pseudo = waitForValidPseudo();
            messageInputHandler.start();
            while (true) {
                waitForMessage();
            }
            // TODO handle multithreading issues
        } catch (IOException e) {
            System.err.println(
                    "Unable to connect to server, please check that server is running or that port is the good one ");
            messageInputHandler.interrupt();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Disconnected from server");


        }

    }

    /**
     * Blocking function that wait for distant messages from distant users connected to the server.
     * it then displays the message using the chat object.
     * @throws IOException,ClassNotFoundException
     */
    private void waitForMessage() throws IOException,ClassNotFoundException {

            ChatMessage message = (ChatMessage) ins.readObject();
            chat.displayMessage(message.getPseudo(), message.getMessage());

    }

    /**
     * Try to send a message to the server
     * @param message content of the message
     * @throws Exception
     */
    private void sendMessage(String message) throws Exception {
            out.writeObject(new ChatMessage(pseudo, message));
            if(message.equals("exit"))
            {
                System.exit(0);
            }
    }

    /**
     * Pseudo ask routine, it waits until a pseudo is typed and respect a number of condition.
     * @return a locally valid pseudo (without space, or non-empty)
     */
    private String askForPseudo() {
        boolean pseudoWellFormatted = false;
        System.out.println("Veuillez saisir votre pseudo :");
        String pseudo = scanner.nextLine();
        do {
            if (pseudo.contains(" ")) {
                System.out.println("Votre pseudo contient un espace. Saisir votre pseudo à nouveau: ");
                pseudo = scanner.nextLine();
            } else if (pseudo.isEmpty()) {
                System.out.println("Votre pseudo est vide. Saisir votre pseudo à nouveau: ");
                pseudo = scanner.nextLine();
            }
            else {
                pseudoWellFormatted=true;
            }
        } while (!pseudoWellFormatted);

        return pseudo;
    }

    /**
     * Send a pseudo validation request to server, to confirm that the pseudo is available,
     * otherwise another pseudo will be asked until it's valid.
     * @return the server validated pseudo
     * @throws Exception
     */
    private String waitForValidPseudo() throws Exception {
        ServerResponse res = null;
        String wantedPseudo = null;
        do {
            if (res != null && res == ServerResponse.PseudoAlreadyInUse) {
                // si le server a dit le pseudo est deja utilisé, on en redemande un et on le
                // dit au client
                System.out.println("Pseudo already in use");
            }
            wantedPseudo = askForPseudo();
            out.writeObject(new PseudoMessage(wantedPseudo));
            System.out.println("Sending pseudo to server");
        } while ((res = (ServerResponse) ins.readObject()) == ServerResponse.PseudoAlreadyInUse);
        return wantedPseudo;
    }
}
