package fr.utc.sr03.devoir1.common;

import java.io.Serializable;

public class ChatMessage implements Serializable {
    String pseudo;
    String message;

    public ChatMessage(String pseudo, String message) {
        this.pseudo = pseudo;
        this.message = message;
    }

    public String getPseudo() {
        return pseudo;
    }

    public String getMessage() {
        return message;
    }

}
