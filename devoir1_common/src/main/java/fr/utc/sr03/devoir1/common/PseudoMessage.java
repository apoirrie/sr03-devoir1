package fr.utc.sr03.devoir1.common;

import java.io.Serializable;

public class PseudoMessage implements Serializable {
    String pseudo;

    public PseudoMessage(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPseudo() {
        return pseudo;
    }
}
