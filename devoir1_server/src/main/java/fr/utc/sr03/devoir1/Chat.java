package fr.utc.sr03.devoir1;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import fr.utc.sr03.devoir1.common.ChatMessage;

/**
 * Handle the creation of the users in the chat, and handle communication between them
 */
public class Chat {
    private final List<ChatClient> users = new ArrayList<ChatClient>();
    private ReentrantLock pseudoCheckerLock = new ReentrantLock();
    private ReentrantLock onUserLeaveLock = new ReentrantLock();
    private ReentrantLock onMessageReceivedLock = new ReentrantLock();


    /**
     * Create a ChatClient instance associated with the socket.
     * @param socket new client socket
     */
    public void addClientToChat(Socket socket) {
        ChatClient chatClient = new ChatClient(socket, this::pseudoAlreadyInUse, this::onUserLeaveChat,
                this::onMessageReceived);
        users.add(chatClient);
        chatClient.start();
        System.out.println("Client trying to join chat");

    }

    /**
     * Inform all users that one client has left the chat, and remove the concerned users from the userlist
     * @param client client that left the chat
     */
    private void onUserLeaveChat(ChatClient client) {
        onUserLeaveLock.lock();
        for (ChatClient chatClient : users) {
            chatClient.sendMessage(new ChatMessage(client.getPseudo(), " left the chat :'("));
        }
        System.out.println(client.getPseudo() + " left the chat :'(");
        users.remove(client);
        onUserLeaveLock.unlock();
    }

    /**
     * dispatch the message sent from one of the client to all other clients
     * @param message Object containing pseudo and content of the message
     */
    private void onMessageReceived(ChatMessage message) {
        onMessageReceivedLock.lock();
        for (ChatClient chatClient : users) {
            chatClient.sendMessage(message);
        }
        onMessageReceivedLock.unlock();
    }

    /**
     * Check the availablity of a pseudo by looking in the users array.
     * @param pseudo pseudo to check
     * @return true if pseudo is already in use, false otherwise
     */
    private boolean pseudoAlreadyInUse(String pseudo) {
        pseudoCheckerLock.lock();
        for (ChatClient chatClient : users) {
            System.out.println(chatClient.getPseudo());
            if (chatClient.getPseudo() != null && chatClient.getPseudo().equals(pseudo)) {
                return true;
            }
        }
        pseudoCheckerLock.unlock();
        return false;
    }
}
