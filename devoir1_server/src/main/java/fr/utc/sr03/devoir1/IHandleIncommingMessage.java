package fr.utc.sr03.devoir1;

import fr.utc.sr03.devoir1.common.ChatMessage;

/**
 * Interface used to handle incomming messages
 */
@FunctionalInterface
public interface IHandleIncommingMessage {
    /**
     * Method prototype to handle the dispatch of the message to other clients
     * @param message incomming message
     */
    void send(ChatMessage message);
}
