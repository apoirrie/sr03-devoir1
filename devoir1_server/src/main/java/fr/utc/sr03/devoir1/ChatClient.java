package fr.utc.sr03.devoir1;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

import fr.utc.sr03.devoir1.common.ChatMessage;
import fr.utc.sr03.devoir1.common.PseudoMessage;
import fr.utc.sr03.devoir1.common.ServerResponse;


/**
 * Serverside instance of a client connected to the chat, it handles the pseudo attribution and the communication process
 * this class is fully abstracted of its environnement (dependency injection). We choosed this architecture because
 * in theory it's easier to test.
 */
public class ChatClient extends Thread {
    private String pseudo = null;
    private Socket socket;
    private ICheckPseudo pseudoChecker;
    private IHandleDisconnect disconnectionHandler;
    private IHandleIncommingMessage incommingMessageHandler;
    private ObjectOutputStream out;
    private ObjectInputStream ins;

    public ChatClient(Socket socket, ICheckPseudo checker, IHandleDisconnect disconnectionHandler,
            IHandleIncommingMessage incommingMessageHandler) {
        this.socket = socket;
        this.pseudoChecker = checker;
        this.disconnectionHandler = disconnectionHandler;
        this.incommingMessageHandler = incommingMessageHandler;
    }

    /**
     * Get pseudo of the client
     * @return pseudo
     */
    public String getPseudo() {
        return pseudo;
    }


    /**
     * send message to the client connected to the socket.
     * @param message pseudo and content
     */
    public void sendMessage(ChatMessage message) {
        try {
            out.writeObject(message);
        } catch (Exception e) {
           System.out.println("Unable to send message");
        }
    }


    /**
     * Main loop for the client. It first creates communication object, then wait for a valid pseudo
     * and finally enter the communication loop.
     */
    @Override
    public void run() {
        try {
            out = new ObjectOutputStream(socket.getOutputStream());
            ins = new ObjectInputStream(socket.getInputStream());
            pseudo = waitForValidPseudo();
            incommingMessageHandler.send(new ChatMessage(pseudo, "A rejoint le chat"));

            while (true) {
                ChatMessage message = (ChatMessage) ins.readObject();
                if (message.getMessage().equals("exit")) {

                    incommingMessageHandler.send(new ChatMessage(pseudo, "A quitté le chat"));
                    disconnectionHandler.handleDisconnect(this);
                    break;
                }
                incommingMessageHandler.send(message);
            }

        } catch (SocketException e) {
            if (e.getMessage().equals("Connection reset")) {
                disconnectionHandler.handleDisconnect(this);
            }
        }

        catch (Exception e) {
            disconnectionHandler.handleDisconnect(this);
        }
    }


    /**
     * Routine for pseudo attribution, it will block until an available pseudo is found
     * @return server validated pseudo
     * @throws Exception
     */
    private String waitForValidPseudo() throws Exception {
        boolean isUsed = false;
        String askedPseudo = null;
        do {
            if (isUsed) {
                System.out.println("Pseudo is used ! ");
                out.writeObject(ServerResponse.PseudoAlreadyInUse);
            }
            PseudoMessage pseudoMessage = (PseudoMessage) ins.readObject();
            askedPseudo = pseudoMessage.getPseudo();
        } while ((isUsed = pseudoChecker.check(askedPseudo)) == true);
        out.writeObject(ServerResponse.PseudoIsValid);
        System.out.println("Pseudo is valid !");
        return askedPseudo;

    }

}
