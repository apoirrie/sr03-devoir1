package fr.utc.sr03.devoir1;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * Main class of the server, it starts a server, creates a chat, and handle user connection
 */
public class ChatServer {
    private ServerSocket server;
    private int port;
    private Chat chat = new Chat();

    public ChatServer(int port) {
        this.port = port;
    }

    /**
     * Start the server to the specified port, and enter an infinite loop to accept client connection until shutdown
     */
    public void start() {
        try {
            server = new ServerSocket(port);
            System.out.println("Server started on port : " + port);
            while (true) {
                Socket socket = server.accept();
                chat.addClientToChat(socket);
            }
        } catch (Exception e) {
            if(e.getMessage().equals("Address already in use (Bind failed)")){
                System.out.println("Port already in use");
            }
            else {
                System.out.println(e.getMessage());
            }


        }

    }
}
