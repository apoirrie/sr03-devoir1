package fr.utc.sr03.devoir1;

/**
 * Interface used to handle user disconnection
 */
@FunctionalInterface
public interface IHandleDisconnect {

    /**
     * Method prototype for handling a user disconnection
     * @param client client that want to be/is disconnected
     */
    void handleDisconnect(ChatClient client);
}
