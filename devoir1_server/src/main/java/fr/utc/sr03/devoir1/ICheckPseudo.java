package fr.utc.sr03.devoir1;

/**
 * Interface used to check pseudo server side
 */
@FunctionalInterface
public interface ICheckPseudo {

    /**
     * Method prototype that pseudo checking should respect
     * @param pseudo pseudo to check
     * @return true is pseudo is used, false otherwise (implementation specific)
     */
    boolean check(String pseudo);
}
