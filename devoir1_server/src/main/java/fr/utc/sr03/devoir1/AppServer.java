package fr.utc.sr03.devoir1;

/**
 * Entry point of the Server app, it simply creates a chat server instance and
 * start it
 *
 */
public class AppServer {
    public static void main(String[] args) {
        ChatServer server = new ChatServer(14002);
        server.start();
    }
}
