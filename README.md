## Contexte
Dans le cadre de ce projet, nous avons développé une application de chat Client/Serveur utilisant des sockets pour permettre des discussions publiques entre un ensemble de participants. L'objectif principal de cette application est de fournir une plateforme de communication en temps réel où les utilisateurs peuvent échanger des messages dans une interface console.

### Objectifs
 L'application utilise un serveur central qui gère les connexions des clients et diffuse les messages à l'ensemble de ceux-ci. 

Cette application doit permettre : 

- Aux utilisateurs de communiquer instantanément en envoyant des messages via le serveur.

- D’offrir un espace de discussion ouvert où les utilisateurs peuvent participer à des conversations avec d'autres membres connectés.

- D’assurer que chaque utilisateur dispose d'un pseudonyme unique pour les identifier.

- De gérer les cas où un client se connecte ou se déconnecte de manière à maintenir l'intégrité de la conversation.

### Concepts
Afin d’atteindre ces objectifs:
- Les sockets sont à utiliser pour permettre la communication entre le serveur et les clients.
- Les threads permettent de gérer la communication à la fois côté serveur et côté client, permettant ainsi une interaction asynchrone entre les participants. 
- Le serveur principal exécute une boucle infinie pour attendre les connexions des clients: lorsqu'une nouvelle connexion est établie, le serveur crée un nouveau thread pour gérer cette connexion tout en continuant à attendre de nouvelles connexions.

## Architecture
### Côté serveur
![Diagramme de classes : Serveur](images/Serveur.png)
L'AppServer permet de créer un ChatServer.
Le ChatServer démarre un serveur, gère les connexions des différents utilisateurs et créer un Chat.
Le Chat gère la création d'utilisateurs et la communication entre les différents utilisateurs.
La classe ChatClient qui hérite de Threads permet de gérer une instance d'un client connecté au Chat. Elle s'occupe de l'attribution des pseudonymes et du processus de communication de chacun d'entre eux. 

### Côté Client
![Diagramme de classes : Client](images/Client.png)
L'appClient permet de créer un Client.
Ce client gère les connections au serveur, la communication des messages au serveur et l'attribution des pseudos. Il créé également un Chat qui permet d'afficher les messages dans le terminal. 

### Choix d'architecture

Notons tout d'abord la création d'un paquet partagé `devoir1_common` qui contient des classes partagées entre le serveur et le client. 
Cela nous permet d'éviter tout problème de sérialisation/désérialisation lors de l'envoi d'objets entre client et serveur

Nous avons également fait le choix d'isoler au maximum les différentes classes, en utilisant des injections de dépendance. 
Notemment coté serveur, ou le `ChatClient` n'a aucune connaissance de l'objet `Chat`. Lui sont passé en paramètre les méthodes de la classe `Chat`
nécessaire au bon fonctionnement de l'application. Cette architecture a pour principal avantage de rendre le code facilement testable et modulable. 
### Diagramme de classe de la bibliothèque partagée
![Diagramme de classes : Client](images/Common.png)

### Multithreading
Avec cette injection de dépendance on peut se retrouver dans un cas ou plusieurs threads essaient d’appeler la même méthode simultanément. Pour éviter ce cas de figure : nous avons implémenté un mécanisme de **mutual exclusion** (Mutex):

```public class Chat 
    private final List<ChatClient> users = new ArrayList<ChatClient>();
    private ReentrantLock pseudoCheckerLock = new ReentrantLock();
    private ReentrantLock onUserLeaveLock = new ReentrantLock();
    private ReentrantLock onMessageReceivedLock = new ReentrantLock();
```
On déclare trois objets ReentrantLock pour gérer la synchronisation des différentes opérations : *pseudoCheckerLock*, *onUserLeaveLock*, et *onMessageReceivedLock*.

```
private void onUserLeaveChat(ChatClient client) {
        onUserLeaveLock.lock();
        for (ChatClient chatClient : users) {
            chatClient.sendMessage(new ChatMessage(client.getPseudo(), " left the chat :'("));
        }
        System.out.println(client.getPseudo() + " left the chat :'(");
        users.remove(client);
        onUserLeaveLock.unlock();
    }
```
Cette méthode est appelée lorsqu'un utilisateur quitte le chat. 
Elle verrouille d'abord le Mutex onUserLeaveLock, ce qui garantit qu'aucun autre thread ne peut exécuter cette méthode simultanément. 
Ensuite, elle parcourt la liste des utilisateurs connectés (users) pour envoyer un message de départ à chacun, puis elle supprime l'utilisateur qui quitte le chat de la liste. 
Enfin, elle déverrouille le Mutex onUserLeaveLock.

```
private void onMessageReceived(ChatMessage message) {
        onMessageReceivedLock.lock();
        for (ChatClient chatClient : users) {
            chatClient.sendMessage(message);
        }
        onMessageReceivedLock.unlock();
    }
```
Cette méthode est appelée lorsqu'un message est reçu dans le chat. 
Elle verrouille d'abord le Mutex onMessageReceivedLock, ce qui empêche d'autres threads d'exécuter cette méthode simultanément. 
Ensuite, elle parcourt la liste des utilisateurs connectés pour envoyer le message à chacun. 
Enfin, elle déverrouille le Mutex onMessageReceivedLock.

```
private boolean pseudoAlreadyInUse(String pseudo) {
        pseudoCheckerLock.lock();
        for (ChatClient chatClient : users) {
            System.out.println(chatClient.getPseudo());
            if (chatClient.getPseudo() != null && chatClient.getPseudo().equals(pseudo)) {
                return true;
            }
        }
        pseudoCheckerLock.unlock();
        return false;
    }
```


Cette méthode vérifie si un pseudo est déjà utilisé par un autre utilisateur dans le chat. 
Elle verrouille d'abord le Mutex pseudoCheckerLock pour empêcher d'autres threads de vérifier simultanément. Ensuite, elle parcourt la liste des utilisateurs pour comparer le pseudo donné avec ceux des utilisateurs déjà présents. 
Enfin, elle déverrouille le Mutex pseudoCheckerLock.

## Validité et unicité du pseudonyme
L'utilisateur entre d'abord son pseudonyme puis on vérifie que celui ci n'est pas vide et qu'il ne contient pas d'espaces.

```
 private String askForPseudo() {
        boolean pseudoWellFormatted = false;
        System.out.println("Veuillez saisir votre pseudo :");
        String pseudo = scanner.nextLine();
        do {
            if (pseudo.contains(" ")) {
                System.out.println("Votre pseudo contient un espace. Saisir votre pseudo à nouveau: ");
                pseudo = scanner.nextLine();
            } else if (pseudo.isEmpty()) {
                System.out.println("Votre pseudo est vide. Saisir votre pseudo à nouveau: ");
                pseudo = scanner.nextLine();
            }
            else {
                pseudoWellFormatted=true;
            }
        } while (!pseudoWellFormatted);

        return pseudo;
    }
```

Une fois que le pseudo est saisi par le client, il est envoyé au serveur via un objet PseudoMessage. 
Le serveur reçoit ce message et vérifie la disponibilité du pseudo via la méthode pseudoAlreadyInUse() de la classe Chat. Cette méthode vérifie si le pseudo est déjà utilisé par un autre client connecté:
```
private boolean pseudoAlreadyInUse(String pseudo) {
        pseudoCheckerLock.lock();
        for (ChatClient chatClient : users) {
            System.out.println(chatClient.getPseudo());
            if (chatClient.getPseudo() != null && chatClient.getPseudo().equals(pseudo)) {
                return true;
            }
        }
        pseudoCheckerLock.unlock();
        return false;
    }
```
Si le pseudo est déjà utilisé, le serveur renvoie au client un message indiquant que le pseudo est déjà en cours d'utilisation (ServerResponse.PseudoAlreadyInUse). Le client est alors invité à saisir un nouveau pseudo.
Ce processus se répète jusqu'à ce qu'un pseudo valide soit saisi et confirmé par le serveur. Une fois que le serveur confirme que le pseudo est unique, le client est autorisé à continuer et peut participer au chat.

## Déconnexion inattendue du serveur ou d'un client

### Déconnexion d'un client

Lorsqu'un client se déconnecte, que ce soit volontairement en tapant "exit" ou en perdant la connexion, la méthode onUserLeaveChat(ChatClient client) de la classe Chat est appelée.
   ```
 private void onUserLeaveChat(ChatClient client) {
        onUserLeaveLock.lock();
        for (ChatClient chatClient : users) {
            chatClient.sendMessage(new ChatMessage(client.getPseudo(), " left the chat :'("));
        }
        System.out.println(client.getPseudo() + " left the chat :'(");
        users.remove(client);
        onUserLeaveLock.unlock();
    }
``` 

Cette méthode parcourt la liste des clients connectés et envoie un message à chacun pour informer que le client qui se déconnecte quitte le chat. Ensuite, elle supprime le client de la liste des utilisateurs.
Avant de supprimer le client de la liste, la méthode onUserLeaveChat appelle la méthode handleDisconnect de l'interface IHandleDisconnect pour gérer la déconnexion du client. 

### Déconnexion du serveur 

``` 
public void start(int port) {
    ...
            while (true) {
                waitForMessage();
            }
           
        } catch (IOException e) {
            System.err.println(
                    "Unable to connect to server, please check that server is running or that port is the good one ");
            messageInputHandler.interrupt();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Disconnected from server");

        }
```
Lorsque le serveur est déconnecté, le client reçoit le message "Unable to connect to server, please check that server is running or that port is the good one". La transmission de message est alors arrêtée. 

## Démarrage du programme 
**1. Récupérer le programme à partir du terminal :**

```
git clone https://gitlab.utc.fr/apoirrie/sr03-devoir1.git 

```
**2. Ouvrir le dossier sr03_devoir1 sur IntelliJ**

**3. Ouvrir et Run AppServer du projet devoir1_server**

**4. Ouvrir et Run un ou plusieurs AppClient du projet devoir1_client**



## Cas d'usages
Plusieurs clients peuvent se connecter et échanger des messages. Lorsqu'un utilisateur quitte la conversation, les autres utilisateurs en sont notifiés :
![Test](images/test1.png)


Si un utilisateur souhaite se connecter avec un pseudo existant, il doit entrer à nouveau un pseudo différent :
![Test](images/TestException3.png)



Si un utilisateur quitte abruptement le chat, les autres utilisateurs sont notifiés :
![Test](images/TestException1.png)


Si le serveur se déconnecte, les utilisateurs en sont notifiés: 
![Test](images/testException2.png)

